/**
 * Inbuilt Demeter features.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.demeter.paper.feature;
