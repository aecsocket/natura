/**
 * Demeter Paper module.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.demeter.paper;
